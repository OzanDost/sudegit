﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmalController : MonoBehaviour {
    // Start is called before the first frame update
    public GameObject sude;
    float moveSpeed;
    public CharacterMovement CM;
    bool grounded = false;
    void Start () {
        sude = GameObject.FindWithTag ("Player");
        CM = sude.GetComponent<CharacterMovement> ();
        moveSpeed = 3f;
    }

    // Update is called once per frame
    void Update () {
        
        if (grounded) {
            float step = moveSpeed * Time.deltaTime;
            transform.position = Vector3.MoveTowards (transform.position,
                new Vector3 (sude.transform.position.x, transform.position.y, 0), step);
        }
    }

    private void OnCollisionEnter2D (Collision2D other) {
        if (other.gameObject.tag == "ground") {
            grounded = true;
        }

    }

}