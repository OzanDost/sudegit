﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmalSpawner : MonoBehaviour {
    float timer = 5f;
    public GameObject amal;
    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () {

        timer -= Time.deltaTime;
        if (timer <= 0) {
            Instantiate (amal, transform.position, Quaternion.identity);
            timer = 2f;
        }
    }
}