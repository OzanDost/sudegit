﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bikbik : MonoBehaviour {
    public CharacterMovement CM;
    float direction;
    public GameObject player;
    // Start is called before the first frame update
    void Start () {
        CM = GameObject.FindWithTag ("Player").GetComponent<CharacterMovement> ();
        player = GameObject.FindWithTag ("Player");
        direction = CM.anim.GetFloat ("LastMoveX");
    }

    // Update is called once per frame
    void Update () {
        if (direction > 0) {
            transform.Translate (player.transform.right * 8f * Time.deltaTime);
        } else {
            transform.Translate (-player.transform.right * 8f * Time.deltaTime);
        }
    }

    private void OnCollisionEnter2D (Collision2D other) {
        
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "amal") {
            Debug.Log("girdik");
            Destroy (gameObject);
            Destroy (other.gameObject);
        }
    }
}