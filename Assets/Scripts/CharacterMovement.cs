using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour {
    // Start is called before the first frame update
    private float inputHori;
    private float inputVerti;
    public float speed = 5f;
    private Rigidbody2D rb;
    public Animator anim;
    private bool grounded = false;
    public bool facingRight = true;
    public Transform bikbik;
    public SimpleHealthBar healthBar;

    public float health;

    void Start () {
        health = 100f;
        anim = GetComponent<Animator> ();
        rb = GetComponent<Rigidbody2D> ();
    }

    private void OnCollisionEnter2D (Collision2D other) {
        if (other.gameObject.tag == "ground") {
            grounded = true;
        }
        if (other.gameObject.tag == "amal") {
            health -= 10;
            healthBar.UpdateBar (health, 100);
            Destroy (other.gameObject);
        }
    }
    private void OnCollisionExit2D (Collision2D other) {
        if (other.gameObject.tag == "ground") {
            grounded = false;
        }
    }

    // Update is called once per frame
    void Update () {

        if (SimpleInput.GetAxis ("Horizontal") != 0) {
            anim.SetBool ("IsWalking", true);
        } else {
            anim.SetBool ("IsWalking", false);
        }
        inputHori = SimpleInput.GetAxis ("Horizontal") * speed * Time.deltaTime;
        inputVerti = SimpleInput.GetAxis ("Vertical") * speed * Time.deltaTime;

        anim.SetFloat ("speedX", inputHori);

        if (SimpleInput.GetButtonDown ("Jump") && grounded) {
            anim.SetBool ("IsGrounded", false);
            rb.AddForce (new Vector2 (0f, 6f), ForceMode2D.Impulse);
        } else {
            anim.SetBool ("IsGrounded", true);
        }

        if (SimpleInput.GetButtonDown ("Shoot")) {

            Transform bullet = Instantiate (bikbik, transform.position, Quaternion.Euler (new Vector3 (0, 0, 1)));

        }
        transform.Translate (inputHori, 0, 0);

        if (health <= 0) {
            health = 100;
            healthBar.UpdateBar(health, 100f);
        }
    }

    private void FixedUpdate () {
        float lastInputX = SimpleInput.GetAxis ("Horizontal");

        if (lastInputX > 0) {
            anim.SetFloat ("LastMoveX", 1f);
        } else if (lastInputX < 0) {
            anim.SetFloat ("LastMoveX", -1f);
        }

    }

}